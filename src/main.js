// import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import currency from "./utils/currency";
import axios from "./plugins/request/http";
Vue.prototype.$axios = axios;
Vue.filter("currency", currency);
Vue.config.productionTip = false;
Vue.prototype.$ELEMENT={size:'mini'}
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
