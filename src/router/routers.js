import Home from "@/views/Home";
import MyTitle from "@/views/Edit";
import Usage from "@/views/Usage";
import NeteaseMusic from "@/views/NeteaseMusic";
import Pictures from "@/components/Pictures";
import TestVue from "@/views/Question";
import StudentView from "components/StudentsView";
import Dogs from "@/views/vuexView/Dogs";
import Products from "@/views/vuexView/Products";
import SearchView from "@/components/vuex-demo/search/SearchView";
import Index from "@/views/Index";
import TodoView from "@/views/vuexView/TodoView";
import Tabs from "components/tutor/Tabs";
import TutorIndex from "@/views/TutorIndex";
import VueTricks from "@/views/VueTricks";

export default [
  {
    path: "/netease",
    name: "netease",
    meta: { icon: "md-color-wand", title: "netease" },
    component: NeteaseMusic
  },

  {
    path: "/",
    component: Index,
      meta: { icon: "md-color-wand", title: "index" },
    children: [
      {
        path: "/home",
        name: "home",
        meta: { icon: "md-color-wand", title: "home" },
        component: Home
      },
      {
        path: "/vue",
        name: "vue", meta: { icon: "md-color-wand", title: "tricks" },
        component: VueTricks
      },
      {
        path: "/about",
        name: "about", meta: { icon: "md-color-wand", title: "about" },
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component() {
          return import(/* webpackChunkName: "about" */ "@/views/About.vue");
        }
      },
      {
        path: "/title",
        name: "MyTitle",
        meta: { icon: "md-color-wand", title: "mytitle" },
        component() {
          return import(/*webpackChunkName:"MyTitle" */ "@/views/Edit.vue");
        }
      },
      {
        path: "/show-weather",
        name: "ShowWeather",
        meta: { icon: "md-color-wand", title: "showweather" },
        component() {
          return import(
            /*webpackChunkName:"MyTitle" */ "@/views/ShowWeather.vue"
          );
        }
      },
      {
        path: "/usage",
        name: "Usage", meta: { icon: "md-color-wand", title: "usage" },
        component: Usage
      },
      {
        path: "/tutor",
        name: "tutor", meta: { icon: "md-color-wand", title: "tutor" },
        component: TutorIndex
      },
      {
        path: "/pictures",
        name: "Pictures",
        component: Pictures
      },
      {
        path: "/test-vue",
        name: "TestVue",
        component: TestVue
      },
      {
        path: "/vuex-demo/student-view",
        name: "StudentView",
        component: StudentView
      },
      {
        path: "/vuex-demo/todo", meta: { icon: "md-color-wand", title: "todo" },
        name: "Todo",
        component: TodoView
      },
      {
        path: "/vuex-demo/dogs",
        name: "dogs",
        component: Dogs
      },
      {
        path: "/vuex-demo/products",
        name: "products",
        component: Products
      },
      {
        path: "/vuex-demo/search",
        name: "search",
        component: SearchView
      }
    ]
  }
];
