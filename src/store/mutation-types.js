//HOME
export const CHANGE_SHOW_STATUS = `changeShowStatus`;
export const CHANGE_ARTICLE_LIST = `changeArticleList`;

export const CHANGE_TOPIC_SHOW = `changeTopicShow`;
export const DISPLAY_ARTICLE = `displayArticle`;
export const CHANGELOGIN = `changeLogin`;
export const CHANGELOGINWAY = `changeLoginWay`;
// 查询组件types
// 查询组件中的 查询信息
export const SEARCH_PARAM_LIST = "SEARCH_PARAM_LIST";
export const SET_SEARCH_LIST = "SET_SEARCH_LIST";
export const GET_SEARCH_LIST = "GET_SEARCH_LIST";
export const GET_SEARCH_GROUP_DATA = "GET_SEARCH_GROUP_DATA";

// 设置查询分组值到state
export const SET_SEARCH_GROUP_VAL = "SET_SEARCH_GROUP_VAL";
export const CLEAR_SEARCH_VAL = "CLEAR_SEARCH_VAL";
export const UPDATE_SEARCH_VAL = "UPDATE_SEARCH_VAL";

export const STORAGE_KEY = "qweerrtyuure";
localStorage.setItem('STORAGE_KEY','1111')

